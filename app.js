const PLAYER_1 = 'blue';
const PLAYER_2 = 'red';
const NUM_ROWS = 7;
const NUM_COLUMNS = 8;
var player_1_turn = true;

$(document).ready(function() {
  let grid = [
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
  ];

  function checkRows(){
    for(let i = 0; i<NUM_ROWS; i++){
      if(grid[i][0] !== ' ' && grid[i][0] === grid[i][1] && grid[i][0] === grid[i][2]
         && grid[i][0] === grid[i][3]){
           return grid[i][0];
      } else if(grid[i][1] !== ' ' && grid[i][1] === grid[i][2] && grid[i][1] === grid[i][3]
            && grid[i][1] === grid[i][4]){
              return grid[i][1];
      } else if(grid[i][2] !== ' ' && grid[i][2] === grid[i][3] && grid[i][2] === grid[i][4]
               && grid[i][2] === grid[i][5]){
              return grid[i][2];
      } else if(grid[i][3] !== ' ' && grid[i][3] === grid[i][4] && grid[i][3] === grid[i][5]
         && grid[i][3] === grid[i][6]){
           return grid[i][3];
      } else if(grid[i][4] !== ' ' && grid[i][4] === grid[i][5] && grid[i][4] === grid[i][6]
         && grid[i][4] === grid[i][7]){
           return grid[i][4];
      }
    }
    return null;
  }

  function checkColumns(){
    for(let i = 0; i<NUM_COLUMNS; i++){
      if(grid[0][i] !== ' ' && grid[0][i] === grid[1][i] && grid[0][i] === grid[2][i]
        && grid[0][i] === grid[3][i]){
          return grid[0][i];
        } else if(grid[1][i] !== ' ' && grid[1][i] === grid[2][i] && grid[1][i] === grid[3][i]
          && grid[1][i] === grid[4][i]){
            return grid[1][i];
        } else if(grid[2][i] !== ' ' && grid[2][i] === grid[3][i] && grid[2][i] === grid[4][i]
          && grid[2][i] === grid[5][i]){
            return grid[2][i];
        } else if(grid[3][i] !== ' ' && grid[3][i] === grid[4][i] && grid[3][i] === grid[5][i]
          && grid[3][i] === grid[6][i]){
            return grid[3][i];
        }
    }
    return null;
  }

  function checkDiagonal(){
    //bottom left to upper right
    for(let col = 0; col<5; col++){
      for(let row = 3; row<NUM_ROWS; row++){
        if(grid[row][col] !== ' ' && grid[row][col] === grid[row-1][col+1]
          && grid[row][col] === grid[row-2][col+2]
          && grid[row][col] === grid[row-3][col+3]){
            return grid[row][col];
          }
      }
    }

    //bottom right to upper left
    for(let row = 0; row<4; row++){
      for(let col = 0; col<5; col++){
        if(grid[row][col] !== ' ' && grid[row][col] === grid[row+1][col+1]
          && grid[row][col] === grid[row+2][col+2]
          && grid[row][col] === grid[row+3][col+3]){
            return grid[row][col];
          }
      }
    }

    return null;
  }


  function isFinished(){
    let winner;

    winner = checkRows();
    if(winner){
      return winner;
    }

    winner = checkColumns();
    if(winner){
      return winner;
    }


    winner = checkDiagonal();
    if(winner){
      return winner;
    }


    return null;
  }

  function reset(){
    grid = [
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
    ];
    for(var i = 0; i<NUM_ROWS; i++){
      for(var j = 0; j<NUM_COLUMNS; j++){
        $('.col[data-i=' + i + '][data-j=' + j + ']').css("background-color", "gainsboro");
      }
    }
  }

  $('.col').click(function() {
    const i = $(this).data('i');
    const j = $(this).data('j');
    //console.log(i + ', ' + j);

    var isFull = true;
    for(var k = 6; k>=0; k--){
      console.log(k + ', ' + j);
      if(grid[k][j] === ' '){
        if(player_1_turn){
          grid[k][j] = PLAYER_1;
          $('.col[data-i=' + k + '][data-j=' + j + ']').css("background-color", PLAYER_1).hide().slideToggle();
          isFull = false;
          player_1_turn = false;
        } else{
          grid[k][j] = PLAYER_2;
          $('.col[data-i=' + k + '][data-j=' + j + ']').css("background-color", PLAYER_2).hide().slideToggle();
          isFull = false;
          player_1_turn = true;
        }
        break;
      }
    }

    if(isFull){
      return;
    }

    var winner = isFinished();
    if(winner){
      alert("The winner is: " + winner);
      reset();
    }

  });

  $('#restart').click(function() {
    reset();

  });
});
